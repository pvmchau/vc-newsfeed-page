<?php
function vc_newsfeed_display_comment() {
  global $user;
  ctools_include('ajax');
  ctools_include('modal');
  // Add CTools' javascript to the page.
  ctools_modal_add_js();
  
  $html = '<div class="attachment">';

  $sql_query = "SELECT DISTINCT(comments.nid)
                FROM node node
                  LEFT JOIN comments comments ON node.nid = comments.nid
                  LEFT JOIN og_ancestry og_ancestry ON comments.nid = og_ancestry.nid
                  LEFT JOIN flag_content flag_content ON og_ancestry.group_nid = flag_content.content_id
                  WHERE (node.type in ('blog_post')) AND (node.status = 1) AND (flag_content.fid in ('2', '3')) AND (flag_content.uid = {$user->uid})
                ORDER BY comments.timestamp DESC
                LIMIT 10";

  $select_query = db_query($sql_query);
  while ($comment_row = db_fetch_object($select_query)) {
    if ($comment_row->nid) {
      if (!$node = node_load($comment_row->nid)) return;

      $comment_query = "SELECT MAX(comments.cid) as cid FROM comments WHERE comments.nid = ({$comment_row->nid})";
      $comment_query = db_fetch_object(db_query($comment_query));
      $comment = _comment_load($comment_query->cid); 
                            
      $add_this = '<a class="addthis_button deactive" href="http://www.addthis.com/bookmark.php?v=250&pub=academicroom" addthis:url="%url">';
      $add_this .= t('Share');
      $add_this .= '</a>';
    
      $social = '<ul class="social_link">';
      $social .= '  <li class="comment">%link_comments</li>';
      $social .= '  <li class="share">%link_share</li>';
      $social .= '  <li class="post_time">%time ' . t('ago') . '</li>';
      $social .= '</ul>';

      $comment_profile = content_profile_load('basic', $comment->uid, '', NULL);
      $auth_name = $user->name;
      $fname = $comment_profile->field_first_name[0]['value'];
      $lname = $comment_profile->field_last_name[0]['value'];
      if (trim($fname) != '') {
        $auth_name = "{$fname} {$lname}";
      }
      $link_uname = l($auth_name, "user/{$comment->uid}");
    
      $relationship = user_relationships_load(array("between" => array($user->uid, $comment->uid), 'rtid' => array(1)));
      if ($relationship) {
        $relship = reset($relationship);
        $link_relationship = l('unfollow', "user/{$user->uid}/relationships/{$relship->rid}/remove", array('attributes' => array('class' => 'user_relationships_popup_link follow', 'rel' => 'nofollow'), 'query' => drupal_get_destination()));
      }
      else {
        $link_relationship = l('follow', "relationship/{$comment->uid}/request/{$user->uid}", array('attributes' => array('class' => 'user_relationships_popup_link follow', 'rel' => 'nofollow'), 'query' => drupal_get_destination()));
      }
      $link_uname = $link_uname . ' (' . $link_relationship . ') ';
      $auth_pic = $comment_profile->field_user_picture;
      $auth_pic = $auth_pic[0]['filepath'];
      if (empty($auth_pic)) {
        $auth_pic = drupal_get_path('module', 'fb_style_dropdown') . '/default.gif';
      }
      $auth_pic = theme('imagecache', 'newsfeed_author_image', $auth_pic);
    
      $link_comments = ctools_ajax_text_button(t('Comments'), "comment/post/div-comment-post/list-comment-comments/div-comment-form-post/{$node->nid}", t('Show Comments'));
      $link_share = url("node/{$node->nid}", array('absolute' => TRUE));
      $link_share = str_replace('%url', $link_share, $add_this);
    
      $node_links = $social;
      $node_links = str_replace('%link_comments', $link_comments, $node_links);
      $node_links = str_replace('%link_share', $link_share, $node_links);
      $node_links = str_replace('%time', format_interval(time() - $comment->timestamp), $node_links);
      
      $comment_body = truncate_utf8($comment->comment, 150, true, true, 7);
        
      $div_comment  = '<div class="newsfeed_comment">';
      $div_comment .= vc_newsfeed_build_comment_detail($node, comment_num_all($node->nid));
      $div_comment .= '</div>';   
      
      $html .= '<div class="videoBlocks homePopular" style="margin:5px;border:0px;padding:0px;">';
      $html .= '  <div class="rowBlock odd">';

      switch ($node->type) {
        //This is for Blog Post Content type.
        case "blog_post":

          $html_old = '
                  <div class="videoBlocks homePopular" style="margin:5px;border:0px;padding:0px;">
                    <div class="rowBlock odd">
                      <div class="frame Blog Post_image">
                        <div class="Blog Post_author_image author_image">
                          ' . l($comment_pic, 'user/' . $comment_uid, array('attributes' => array('target' => "_blank"), 'html' => TRUE,)) . '
                        </div>
                      </div>

                      <div class="details">
                        <div class="createbyauthor" style="margin-bottom:10px;">' . $comment_link_u_fullname . ' commented on a blog</div>
                          <div class="info">
                            <h3>' . l($node->title, 'node/' . $nid) . '</h3>
                            <div style="margin-bottom:6px;">' . $comment_body . '</div>
                            <div style="margin-bottom:6px;" class="social-links">' . $comment_created_date . '</div>
                          </div>
                      </div>
                      <div class="newsfeed_comment">' . $comment . '</div>
                    </div>
                  </div>
                  ';

          $html .= vc_newsfeed_display_comment__frame($node, $auth_pic, $class = 'Blog Post_image');
          vc_newsfeed_display_comment__details_group_item($html, $node, $node_links, $link_uname, $suffix = 'commented on a blog', $comment_body);
          break;
      }
      
      $html .= $div_comment;
      $html .= '</div>';
      $html .= '</div>';
    }
  }
  
  $html .= '</div>';
  return $html;
}

function vc_newsfeed_display_comment__frame($comment, $auth_pic, $class = '') {
  $output  = '<div class="frame '. $class .'">';
  $output .= '  <div class="Video_author_image author_image">';
  $output .= '    ' . l($auth_pic, "user/{$comment->uid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}

function vc_newsfeed_build_comment_detail($node, $node_comment_count = 0) {
  $link_comments = ctools_ajax_text_button(t('Comments'), "comment/post/div-comment-post/list-comment-comments/div-comment-form-post/{$node->nid}", t('Show Comments'));
  $div_comment = '<div id="div-comment-post-' . $node->nid . '">';
  if ($node_comment_count) {
    $view_content = views_embed_view('newsfeed_comment', 'block_1', array($node->nid));
    if (!empty($view_content)) {
      $link_load_all = "comment/list/all/list-comment-comments/{$node->nid}/0";
      $link_load_all = ctools_ajax_text_button(t("Show all ({$node_comment_count})"), $link_load_all, t('Show all comments'));
      if ($node_comment_count < 3) {
        $link_load_all = '';
      }
      $div_comment .= ' <div class="comment_header">Recent comments' . $link_load_all . '</div>';
      $div_comment .= '<div id="list-comment-comments-' . $node->nid . '">' . $view_content . '</div>';
      $div_comment .= '</div>';
    }
  } else {
    $div_comment .= '<div class="comment_header">' . t('Recent comments') . '</div>';
    $div_comment .= '<div id="list-comment-comments-' . $node->nid . '"></div>';
    $div_comment .= '</div>';
  }
  $div_comment .= '<div id="div-comment-form-post-' . $node->nid . '">' . drupal_get_form('comments_ajax_comment_form', $node->nid, 'list-comment-comments') . '</div>';
  return $div_comment;
}

function vc_newsfeed_display_comment__details_group_item(&$html, $node, $node_links, $link_uname, $suffix, $comment_body) {
  $html .= ' <div class="details">';
  $html .= '   <div class="createbyauthor" style="margin-bottom:10px;">' . $link_uname . ' '. $suffix .'</div>';
  $html .= '   <div class="info">';
  $html .= '     <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
  $html .= '     <div style="margin-bottom:6px;">' . $comment_body . '</div>';
  $html .= '     <div style="margin-bottom:6px;" class="social-links">' . $node_links . '</div>';
  $html .= '   </div>';
  $html .= ' </div>';
}
