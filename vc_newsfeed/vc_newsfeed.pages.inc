<?php

function vc_newsfeed_display_get_params() {
  global $user;

  // How many nodes per page
  $nf_types = array();

  // Fetch the users basic content types
  if (!$profile = content_profile_load('basic', $user->uid, '', NULL))
    return drupal_not_found();
  if (!count($profile->taxonomy))
    return t('Found no discipline.');

  foreach ($profile->taxonomy as $key => $values) {
    if ($values->vid == 2) {
      $term_ids[] = $values->tid;
    }
  }

  if (!count(!$term_ids))
    return t('Found no discipline.');
  $disciplines = discipline_get_lists($term_ids, 2);

  // Fetch discipline array of logged in user.
  $_disciplines = array();

  foreach ($disciplines as $dis) {
    $a = end($dis);
    $_disciplines[] = $a['value'];
  }

  // Fetch Valid content types that should be displayed on Newsfeed.
  $types = variable_get('newsfeed_content_type', array());
  $types = array_values(array_unique($types));

  foreach ($types as $keys => $values) {
    if (trim($values) != '0') {
      $nf_types[] = trim($values);
    }
  }

  if (count($nf_types))
    $nf_types = implode("', '", $nf_types);
  if (count($_disciplines))
    $_disciplines = implode(',', $_disciplines);

  return array($nf_types, $_disciplines);
}

function vc_newsfeed_display_build_query() {
  global $user;
  // Get query params
  if (!$params = vc_newsfeed_display_get_params())
    return $params;
  $limit = variable_get('newsfeed_pager_count', 10);
  list($nf_types, $_disciplines) = $params;

  if (!empty($_GET['debug']) && $_GET['debug'] === 'video') {
    $nf_types = 'video';
  }

  $sql_count_dev = "SELECT COUNT(DISTINCT(node.nid))
                FROM node node
                  LEFT JOIN term_node term_node ON node.nid = term_node.nid
                  LEFT JOIN og_ancestry og_ancestry ON node.nid = og_ancestry.nid
                  LEFT JOIN flag_content flag_content ON og_ancestry.group_nid = flag_content.content_id
                WHERE node.type in ('{$nf_types}')
                  AND node.status = 1
                  AND (term_node.tid in ({$_disciplines}) OR (flag_content.fid in ('2', '3') AND flag_content.uid = {$user->uid}))";
                  
  $sql_query_dev = "SELECT DISTINCT(node.nid) AS nid
                FROM node node
                  LEFT JOIN term_node term_node ON node.nid = term_node.nid
                  LEFT JOIN og_ancestry og_ancestry ON node.nid = og_ancestry.nid
                  LEFT JOIN flag_content flag_content ON og_ancestry.group_nid = flag_content.content_id
                WHERE node.type in ('{$nf_types}')
                  AND node.status = 1
                  AND (term_node.tid in ({$_disciplines}) OR (flag_content.fid in ('2', '3') AND flag_content.uid = {$user->uid}))
                ORDER BY node.created DESC";
                
  $sql_count = "SELECT COUNT(DISTINCT(node.nid))
                FROM node node
                  LEFT JOIN term_node term_node ON node.nid = term_node.nid          
                WHERE node.type in ('{$nf_types}')
                  AND node.status = 1
                  AND term_node.tid in ({$_disciplines})";
                  
  $sql_query = "SELECT DISTINCT(node.nid) AS nid
                FROM node node
                  LEFT JOIN term_node term_node ON node.nid = term_node.nid
                WHERE node.type in ('{$nf_types}')
                  AND node.status = 1
                  AND term_node.tid in ({$_disciplines}) 
                ORDER BY node.created DESC";
  
  return pager_query($sql_query, $limit, 0, $sql_count);
}

function vc_newsfeed_comment_num_all($nids) {
  $comment_count = array();
  if (count($nids)) {
    $nids = implode(",", $nids);
    $query = db_query("SELECT ncs.nid, ncs.comment_count FROM {node_comment_statistics ncs} WHERE ncs.nid in ({$nids})");

    while ($node = db_fetch_object($query)) {
      $comment_count[$node->nid] = $node->comment_count ;    
    }
  }
  return $comment_count;
}

function vc_newsfeed_display() {
  global $user;

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();
  drupal_add_js(drupal_get_path('module', 'newsfeed') . '/js/newsfeed.js');

  $html = '';

  $query = vc_newsfeed_display_build_query();
  
  $query_result = $query;
  while ($node = db_fetch_object($query_result)) {
    $nids[] = $node->nid;
  }
  $comment_count = vc_newsfeed_comment_num_all($nids);
  
  foreach ($nids as $node) {
    if (isset($comment_count[$node])) {
      vc_newsfeed_display_node($html, $node, $user, $comment_count[$node]);
    }
    else {
      vc_newsfeed_display_node($html, $node, $user, 0);
    }
  }

  if (!$_GET['page']) {
    module_load_include('inc', 'vc_newsfeed', 'vc_newsfeed.comment');
    $html .= vc_newsfeed_display_comment(); 
  }
  
  $html .= '<div style="margin:5px;">' . theme('pager', NULL, $limit) . '</div>';
  $html .= '<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pub=academicroom"></script>';
  $html = '<div class="view-newsfeed">' . $html . '</div>';

  return $html;
}

function vc_newsfeed_profile_info($uid) {
  if (!$cache = cache_get('profile_info_'.$uid)) {
    $sql_query = "SELECT u.name, ctb.field_first_name_value, ctb.field_last_name_value, f.filepath
                    FROM node n 
                      INNER JOIN users u ON u.uid = n.uid 
                      INNER JOIN node_revisions r ON r.vid = n.vid
  		                INNER JOIN content_type_basic ctb ON ctb.nid = n.nid
  		                LEFT JOIN files f ON f.fid = ctb.field_user_picture_fid
                    WHERE n.type = 'basic'  
                      AND n.uid = '{$uid}'";
    $result = db_fetch_object(db_query($sql_query));
    cache_set('profile_info_'.$uid, $result, 'cache', REQUEST_TIME  + (3600 * 24 * 30));
    return $result;
  } else {
    return $cache->data;
  }
}

function vc_newsfeed_build_comment($node, $node_comment_count = 0) {
  $output = '<div id="div-group-post-' . $node->nid . '">';
  if ($node_comment_count) {
    $view_content = views_embed_view('newsfeed_comment', 'block_1', array($node->nid));
    if (!empty($view_content)) {
      $link_load_all = "comment/list/all/list-group-comments/{$node->nid}/0";
      $link_load_all = ctools_ajax_text_button(t("Show all ({$node_comment_count})"), $link_load_all, t('Show all comments'));
      if ($node_comment_count < 3) {
        $link_load_all = '';
      }
      $output .= ' <div class="comment_header">Recent comments' . $link_load_all . '</div>';
      $output .= '<div id="list-group-comments-' . $node->nid . '">' . $view_content . '</div>';
      $output .= '</div>';
    }
  } else {
    $output .= '<div class="comment_header">' . t('Recent comments') . '</div>';
    $output .= '<div id="list-group-comments-' . $node->nid . '"></div>';
    $output .= '</div>';
  }
  $output .= '<div id="div-group-form-post-' . $node->nid . '">' . drupal_get_form('comments_ajax_comment_form', $node->nid, 'list-group-comments') . '</div>';
  return $output;
}

function vc_newsfeed_display_node(&$html, $node, $user, $node_comment_count = 0) {
  if (!isset($user)) {
    global $user;
  }  
  
  $add_this = '<a class="addthis_button deactive" href="http://www.addthis.com/bookmark.php?v=250&pub=academicroom" addthis:url="%url">';
  $add_this .= t('Share');
  $add_this .= '</a>';

  $social = '<ul class="social_link">';
  $social .= '  <li class="comment">%link_comments</li>';
  $social .= '  <li class="share">%link_share</li>';
  $social .= '  <li class="post_time">%time ' . t('ago') . '</li>';
  $social .= '</ul>';

  if (!$cache = cache_get('newsfeed_node_load_'.$node)) {
    $node = node_load($node);
    cache_set('newsfeed_node_load_'.$node->nid, $node, 'cache', REQUEST_TIME  + (3600 * 24 * 30));
  } else {
    $node = $cache->data;
  }
  if (!$node) return;
  $profile_info = vc_newsfeed_profile_info($node->uid);

  $auth_name = $profile_info->name;
  $fname = $profile_info->field_first_name_value;
  $lname = $profile_info->field_last_name_value;
  if (trim($fname) != '') {
    $auth_name = "{$fname} {$lname}";
  }
  
  $link_uname = l($auth_name, "user/{$node->uid}");

  $relationship = user_relationships_load(array("between" => array($user->uid, $node->uid), 'rtid' => array(1)));
  if ($relationship) {
    $relship = reset($relationship);
    $link_relationship = l('unfollow', "user/{$user->uid}/relationships/{$relship->rid}/remove", array('attributes' => array('class' => 'user_relationships_popup_link follow', 'rel' => 'nofollow'), 'query' => drupal_get_destination()));
  }
  else {
    $link_relationship = l('follow', "relationship/{$node->uid}/request/{$user->uid}", array('attributes' => array('class' => 'user_relationships_popup_link follow', 'rel' => 'nofollow'), 'query' => drupal_get_destination()));
  }
  $link_uname = $link_uname . ' (' . $link_relationship . ') ';

  $auth_pic = $profile_info->filepath;  
  if (empty($auth_pic)) {
    $auth_pic = drupal_get_path('module', 'fb_style_dropdown') . '/default.gif';
  }
  $auth_pic = theme('imagecache', 'newsfeed_author_image', $auth_pic);
  
  $link_comments = ctools_ajax_text_button(t('Comments'), "comment/post/div-content-post/list-content-comments/div-content-form-post/{$node->nid}", t('Show Comments'));
  $div_comment = '<div class="newsfeed_comment">';
  $div_comment .= '  <div id="div-content-post-' . $node->nid . '"></div>';
  $div_comment .= '  <div id="div-content-form-post-' . $node->nid . '"></div>';
  $div_comment .= '</div>';

  if (in_array($node->type, array('blog_post', 'discussions'))) {
    $link_comments = ctools_ajax_text_button(t('Comments'), "comment/post/div-group-post/list-group-comments/div-group-form-post/{$node->nid}", t('Show Comments'));
    $div_comment  = '<div class="newsfeed_comment">';
    $div_comment .= vc_newsfeed_build_comment($node, $node_comment_count);
    $div_comment .= '</div>';
  }

  $link_share = url("node/{$node->nid}", array('absolute' => TRUE));
  $link_share = str_replace('%url', $link_share, $add_this);

  $node_links = $social;
  $node_links = str_replace('%link_comments', $link_comments, $node_links);
  $node_links = str_replace('%link_share', $link_share, $node_links);
  $node_links = str_replace('%time', format_interval(time() - $node->created), $node_links);

  $html .= '<div class="videoBlocks homePopular" style="margin:5px;border:0px;padding:0px;">';
  $html .= '  <div class="rowBlock odd">';

  switch ($node->type) {
    case 'video':
      $embed = '';

      if (!empty($node->field_embedded_video[0]['data']['raw']['MEDIA:GROUP']['YT:VIDEOID']['0'])) {
        $video_id = $node->field_embedded_video[0]['data']['raw']['MEDIA:GROUP']['YT:VIDEOID']['0'];

        $embed .= '<object width="450" height="350">';
        $embed .= ' <param name="movie" value="http://www.youtube.com/v/' . $video_id . '"></param>';
        $embed .= ' <embed src="http://www.youtube.com/v/' . $video_id . '" type="application/x-shockwave-flash" width="450" height="350" wmode="transparent"></embed>';
        $embed .= '</object><br />';
        $thumb = $node->field_embedded_video[0]['data']['thumbnail']['url'];
        $thumb = '<img class="imagecache imagecache-newsfeed_video_preview" src="' . $thumb . '" width="250" height="200" />';
      }

      $html .= '    ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Video_image');
      $html .= '    <div class="details">';
      $html .= '      <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_directorproducer');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Video Lecture');
      $html .= '      <div class="media-embed" style="margin-bottom:10px;">';
      $html .= '        ' . $thumb;
      $html .= '        <div class="play_button"></div>';
      $html .= '        <div class="emvideo emvideo-preview emvideo-youtube" style="display: block;">';
      $html .= '          ' . $embed;
      $html .= '        </div>';
      $html .= '      </div>';
      $html .= '      <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '    </div>';
      break;

    // This is for Book Content type.
    case 'aca_book':
      if (trim($node->field_isbn_book[0]['value']) != '') {        
      //  drupal_add_js('getGoogleLibraryData(' . $node->field_isbn_book[0]['value'] . ');', 'inline');
      }

      $frame = vc_newsfeed_display_node__frame_img(
                $node,
                $field = 'field_acad_book_thumbnail',
                $class = 'Book_image',
                $default = 'sites/all/themes/academicroom/images/book-default-image.png',
                $id = 'field_isbn_book');

      $html .= $frame;
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_acad_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Book');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    case 'article':
      $frame = vc_newsfeed_display_node__frame_img(
                $node,
                $field = '',
                $class = 'Article_image',
                $default = 'sites/all/themes/academicroom/images/book-default-image.png');

      $html .= $frame;
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Article');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    case 'dissertation':
      $frame = vc_newsfeed_display_node__frame_img(
                $node,
                $field = '',
                $class = 'Article_image',
                $default = 'sites/all/themes/academicroom/images/book-default-image.png');

      $html .= $frame;
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_acad_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Dissertation');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    case 'images':
      $img = '';
      if ($node->field_image_upload[0]['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_image_preview', $node->field_image_upload[0]['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
      }
      elseif ($node->field_image_embed[0]['data']['emthumb']['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_image_preview', $node->field_image_embed[0]['data']['emthumb']['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
      }

      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Images_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_artist');
      $html .= '    <div style="margin-bottom:10px;">';
      $html .= '      Photo added by ' . $link_uname . ' ' . vc_newsfeed_get_full_discipline($node->taxonomy, $node->nid);
      $html .= '    </div>';
      $html .= '    <div class="media-embed" style="float:left;margin-bottom:6px;">' . $img . '</div>';
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    //This is for Course lecture Content type.
    case 'course_lecture':
      $img = '';
      if ($node->field_course_lecture_thumbnail[0]['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_profile_image', $node->field_course_lecture_thumbnail[0]['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
      }

      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Lectures_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_course_lecture_authors');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Course Lecture');
      $html .= '    <div style="margin-bottom:6px;">' . $img . '</div>';
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    //This is for Groups Content type.
    case 'groups':
      $img = '';
      if ($node->field_group_logo[0]['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_group_thumbnail', $node->field_group_logo[0]['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE,));
      } else {
        $img = "sites/all/themes/academicroom/images/group2.jpg";
        $img = l(theme('imagecache', 'newsfeed_group_thumbnail', $img), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE,));
      }

      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Groups_image');
      $html .= '  <div class="details">';
      $html .= '    <div class="createbyauthor" style="margin-bottom:10px;">' . $link_uname . ' started a group.</div>';
      $html .= '    <div>';
      $html .= '      <div class="logo">' . $img . '</div>';
      $html .= '      <div class="info">';
      $html .= '        <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      $html .= '        <div style="margin-bottom:6px;">' . vc_newsfeed_get_full_discipline($node->taxonomy, $node->nid) . '</div>';
      $html .= '      </div>';
      $html .= '      <div style="margin-bottom:6px;" class="social-links">' . $node_links . '</div>';
      $html .= '    </div>';
      $html .= '  </div>';
      break;

    case 'book_review':
      if (trim($node->field_isbn_book[0]['value']) != '') {
      //  drupal_add_js('getGoogleLibraryData(' . $node->field_isbn_book[0]['value'] . ');', 'inline');
      }

      $html .= '  ' . vc_newsfeed_display_node__frame_book_review($node);
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_acad_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Book');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    // This is for Book Chapter type.
    case 'book_chapter':
      if (trim($node->field_isbn_book[0]['value']) != '') {
      //  drupal_add_js('getGoogleLibraryData(' . $node->field_isbn_book[0]['value'] . ');', 'inline');
      }

      $html .= '  ' . vc_newsfeed_display_node__frame_book_review($node, $class = 'Chapter_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_acad_author');
      if ($node->field_acad_editor[0]['value'] != '') {
        $html .= '  <div class="byauthor" style="margin-bottom:6px;">';
        $html .= '    Edited by ' . vc_newsfeed_get_author_full_name($node->field_acad_editor, $node->nid);
        $html .= '  </div>';
      }
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Book Chapter');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    //This is for Audio Content type.
    case 'audio':
      $embed = $node->field_embed_audio['0']['value'];
      if (empty($node->field_embed_audio['0']['value'])) {
        $audio_file_url = file_create_url($node->field_audio_file['0']['filepath']);
        if (module_exists('mp3player')) {
          $embed = theme('mp3player', 'Default', $audio_file_url, $audio_title, $audio_artist, $description);
        }
      }

      $img = '';
      if ($node->field_audio_image[0]['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_profile_image', $node->field_audio_image[0]['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE,));
      }

      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Video_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_performer');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Audio');
      $html .= '    <div style="float:left;margin-bottom:6px;width:100%">' . $img . '</div>';
      $html .= '    <div style="float:left;margin-bottom:6px;">' . $embed . '</div>';
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    // This is for Manuscript type.
    case 'manuscripts':
      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Manuscript_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_acad_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Manuscripts');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    //This is for bibliographies type.
    case "syllabus":
      $frame = vc_newsfeed_display_node__frame_img(
                $node,
                $field = '',
                $class = 'Syllabus_image',
                $default = 'sites/all/themes/academicroom/images/book-default-image.png');

      $html .= $frame;
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__author($html, $node, 'field_syllabus_author');
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Syllabus');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    // This is for bibliographies type.
    case 'webpage':
      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Webpage_image');
      $html .= '  <div class="details">';
      $html .= '    <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      vc_newsfeed_display_node__poster($html, $node, $link_uname, 'Webpage');
      $html .= '    <div style="margin-bottom:6px;">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    case 'blog':
      $img = '';
      if ($node->field_blog_logo[0]['filepath'] != '') {
        $img = l(theme('imagecache', 'newsfeed_group_thumbnail', $node->field_blog_logo[0]['filepath']), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE,));
      } else {
        $img = "sites/all/themes/academicroom/images/blog_default_image.png";
        $img = l(theme('imagecache', 'newsfeed_group_thumbnail', $img), "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE,));
      }

      $html .= '  ' . vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Blog_image');
      $html .= '  <div class="details">';
      $html .= '    <div class="createbyauthor" style="margin-bottom:10px;">' . $link_uname . ' started a blog.</div>';
      $html .= '    <div class="logo">' . $img . '</div>';
      $html .= '    <div class="info">';
      $html .= '      <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
      $html .= '      <div style="margin-bottom:6px;">' . vc_newsfeed_get_full_discipline($node->taxonomy, $node->nid) . '</div>';
      $html .= '    </div>';
      $html .= '    <div style="margin-bottom:6px;" class="social-links">' . $node_links . '</div>';
      $html .= '  </div>';
      break;

    //This is for Blog Post Content type.
    case 'blog_post':
      $html .= vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Blog Post_image');
      vc_newsfeed_display_node__details_group_item($html, $node, $node_links, $link_uname, $suffix = 'posted on a blog');
      break;

    case 'discussions':
      $html .= vc_newsfeed_display_node__frame($node, $auth_pic, $class = 'Discussions_image');
      vc_newsfeed_display_node__details_group_item($html, $node, $node_links, $link_uname, $suffix = 'started a group discussions');
      break;
  }
  
  $html .= $div_comment; 
  $html .= '</div>';

  $html .= '</div>';
}

function vc_newsfeed_display_node__frame($node, $auth_pic, $class = '') {
  $output  = '<div class="frame '. $class .'">';
  $output .= '  <div class="Video_author_image author_image">';
  $output .= '    ' . l($auth_pic, "user/{$node->uid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}

function vc_newsfeed_display_node__frame_img($node, $field = '', $class, $default, $id = '') {
  $img = $default;

  if (!empty($field)) {
    if (!empty($node->{$field}[0]['filepath'])) {
      $img = $node->{$field}[0]['filepath'];
    }
  }

  $img = theme('imagecache', 'newsfeed_default', $img, $node->title, strip_tags($node->title));
  $img = l($img, "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));

  if (!empty($id)) {
    $id  = $node->{$id}[0]['value'];
    $img = '<span id="bookImg_'. $id .'" class="book-thumbnail">'. $img .'</span>';
  }

  $output  = '<div class="frame '. $class .'">';
  $output .= '  ' . $img;
  $output .= '</div>';
  return $output;
}

function vc_newsfeed_display_node__frame_book_review($node, $class = 'Review_image') {
  $img = 'sites/all/themes/academicroom/images/book-default-image.png';
  $img = theme('imagecache', 'newsfeed_default', $img, $node->title, strip_tags($node->title));
  if ($node->field_book_cover[0]['value']) {
    $cover_value = explode(',', $node->field_book_cover[0]['value']);
    $img = '<img src="http://covers.openlibrary.org/b/id/' . $cover_value[0] . '-M.jpg" width="58" height="79" />';
  }

  $output  = '<div class="frame Book '. $class .'">';
  $output .= '  <span class="Book Review_author_image" id="bookImg_' . $node->field_isbn_book[0]['value'] . '">';
  $output .= '    ' . l($img, "node/{$node->nid}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
  $output .= '  </span>';
  $output .= '</div>';
  return $output;
}

function vc_newsfeed_display_node__author(&$html, $node, $field) {
  if (!empty($node->{$field}) && $node->{$field}[0]['value'] != '') {
    $html .= '  <div class="byauthor" style="margin-bottom:6px;">';
    $html .= '    By ' . vc_newsfeed_get_author_full_name($node->{$field}, $node->nid);
    $html .= '  </div>';
  }
}

function vc_newsfeed_display_node__poster(&$html, $node, $link_uname, $prefix) {
  $html .= '    <div class="addby" style="margin-bottom:10px;">';
  $html .= '      '. $prefix .' added by ' . $link_uname . ' ' . vc_newsfeed_get_full_discipline($node->taxonomy, $node->nid);
  $html .= '    </div>';
}

function vc_newsfeed_display_node__details_group_item(&$html, $node, $node_links, $link_uname, $suffix) {
  $group_id = reset($node->og_groups);
  $group_name = reset($node->og_groups_both);

  $html .= ' <div class="details">';
  $html .= '   <div class="createbyauthor" style="margin-bottom:10px;">' . $link_uname . ' '. $suffix .'</div>';
  $html .= '   <div class="addby" style="margin-bottom:10px;">';
  $html .= '     ' . l($group_name, "node/{$group_id}", array('attributes' => array('target' => "_blank"), 'html' => TRUE));
  $html .= '     ' . vc_newsfeed_get_full_discipline(node_load($group_id)->taxonomy, $group_id) . ' ' . vc_newsfeed_get_full_discipline($node->taxonomy, $node->nid);
  $html .= '   </div>';
  $html .= '   <div class="info">';
  $html .= '     <h3>' . l($node->title, "node/{$node->nid}") . '</h3>';
  $html .= '     <div style="margin-bottom:6px;" class="social-links">' . $node_links . '</div>';
  $html .= '   </div>';
  $html .= ' </div>';
}

function vc_newsfeed_get_author_full_name($author_array = array(), $nid = 0) {
  if (!$cache = cache_get('full_name_'.$nid)) {
    $arr = $author_array;
    $arr_count = count($arr);
    $arr_one = array();
    $arr_two = array();
  
    if ($arr_count) {
      foreach ($arr as $key => $values) {
        $cleaned_author_array[] = $values['value'];
      }
      $cleaned_author_array = array_filter($cleaned_author_array);
      if (count($cleaned_author_array) != 0) {
        $author_string_to_print = '';
        $ct = count($cleaned_author_array);
        for ($i = 0; $i < $ct; $i++) {
          $arr_query[] = "'" . addslashes(trim($cleaned_author_array[$i])) . "'";
          $arr_one["'" . $cleaned_author_array[$i] . "'"] = trim($cleaned_author_array[$i]);
        }
  
        $arr_query = implode(',', $arr_query);
        $user_select_query = 'SELECT DISTINCT(users.uid), users.name, content_type_basic.field_first_name_value, content_type_basic.field_last_name_value
                                FROM {users}, {node}, {content_type_basic}
                                WHERE  node.nid = content_type_basic.nid AND node.uid = users.uid AND users.name IN (' . $arr_query . ')
                                ORDER BY content_type_basic.field_first_name_value, content_type_basic.field_last_name_value DESC';
  
        $count_user_select_query = 'SELECT COUNT(DISTINCT(users.uid)) FROM {users}, {node}, {content_type_basic} WHERE  node.nid = content_type_basic.nid AND node.uid = users.uid AND users.name IN (' . $arr_query . ')';
  
        $result_user_select_query = db_query($user_select_query);
        $result_count_user_select_query = db_result(db_query($count_user_select_query));
  
        if ($result_count_user_select_query) {
          while ($user_row = db_fetch_array($result_user_select_query)) {
            $fname = trim($user_row['field_first_name_value']);
            $lname = trim($user_row['field_last_name_value']);
            $arr_two["'" . $user_row['name'] . "'"] = l($fname . ' ' . $lname, 'user/' . $user_row['uid']);
          }
        }
  
        $final_author_array = array_merge($arr_one, $arr_two);
        $author_string_to_print = implode(', ', $final_author_array);
        
        cache_set('full_name_'.$nid, $author_string_to_print, 'cache', REQUEST_TIME  + (3600 * 24 * 30));
        return $author_string_to_print;
      }
    }
  } else {
    return $cache->data;
  }
}

function vc_newsfeed_get_full_discipline($taxonomy_array = array(), $nid = 0) {   
  if (!$cache = cache_get('full_discipline_'.$nid)) {
    $html = '';
    $i = 0;
    if (count($taxonomy_array) > 0) {
      foreach ($taxonomy_array as $key => $values) {
        if ($values->vid == 2) {
          $term_ids[$i] = $values->tid;
          $i++;
        }
      }
      
      $dis = discipline_get_lists($term_ids, 2);      
      $html = 'in ';
      $term_count = count($dis);
      if ($term_count > 1) {
        $term_count--;
        $top_dis = reset($dis);
  
        $parent_dis = reset($top_dis);
        $last_child_dis = end($top_dis);
        $html .= l($parent_dis['label'], 'taxonomy/term/' . $parent_dis['value']);
        if ($parent_dis['value'] != $last_child_dis['value']) {
          $html .= ' : ';
          $html .= l($last_child_dis['label'], 'taxonomy/term/' . $last_child_dis['value']);
        }
        unset($dis[0]);
  
        $html .= ' and ';
        if ($term_count == 1) {
          $html .= '<span class=other>' . $term_count . ' other <div class="term count">';
        } else {
          $html .= '<span class=other>' . $term_count . ' others <div class="term count">';
        }      
        foreach ($dis as $key => $values) {
          $last_child_dis = end($values);
          $html .= l($last_child_dis['label'], 'taxonomy/term/' . $last_child_dis['value']);
        }
        $html .= '</div><div class="tooltip-icon"></div><div class="close"></div></span>';
      } elseif ($term_count == 1) {
        $top_dis = reset($dis);
        $parent_dis = reset($top_dis);
        $last_child_dis = end($top_dis);
        $html .= l($parent_dis['label'], 'taxonomy/term/' . $parent_dis['value']);
        if ($parent_dis['value'] != $last_child_dis['value']) {
          $html .= ': ';
          $html .= l($last_child_dis['label'], 'taxonomy/term/' . $last_child_dis['value']);
        }
      }
    }
    cache_set('full_discipline_'.$nid, $html, 'cache', REQUEST_TIME  + (3600 * 24 * 30));
    return $html;
  } else {
    return $cache->data;
  }
}
