<?php

function vc_newsfeed_admin_settings() {
  //Fetch all the Content types on the website and display in the Drop Down.
  $node_types = node_get_types('names');

  $form['newsfeed_content_type'] = array(
    '#type' => 'checkboxes',
    '#options' => $node_types,
    '#title' => t('Content types for Newsfeed.'),
    '#default_value' => variable_get('newsfeed_content_type', array()),
    '#description' => t('Choose the content types which should be displayed on the Newsfeed on Academic Room website to the authenticated users on their landing pages as per their disciplines.'),
    '#required' => TRUE,
  );

  $form['newsfeed_pager_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of updates per page on Newsfeed'),
    '#default_value' => variable_get('newsfeed_pager_count', 10),
    '#description' => t("Enter the number of rows/updates you want displayed per page on the listing pages (support incedents, gold contracts etc)'. "),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
