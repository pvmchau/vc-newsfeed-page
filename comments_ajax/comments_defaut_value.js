(function ($) {

Drupal.behaviors.cff = function(context) {
  var text = Drupal.t('Reply or add a comment');

  $('.newsfeed_comment .form-textarea')
    .each(function(){
      $(this)
        .focus(function(){
            $(this).val('')
              .parents('form:eq(0)').find('.form-submit').attr('disabled', '');
        })
        .blur(function(){
            var v = $(this).val();
            if (v == '') {
              $(this).val(text)
                .parents('form:eq(0)').find('.form-submit').attr('disabled', 'disabled');
            }
        })
        .val(text)
        .parents('form:eq(0)')
          .find('.form-submit').attr('disabled', 'disabled').end()
          .end();
    });
};

})(jQuery);
