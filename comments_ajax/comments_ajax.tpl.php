<?php
/**
 * List variables
 * $comment
 * $user
 * $avatar
 * $profile
 */
 
$url = url("node/{$comment->nid}", array('absolute' => TRUE, 'fragment' => "comment-{$comment->cid}"));
$url = urlencode($url);
?>

<div class="view view-newsfeed-comment view-id-newsfeed_comment view-display-id-block_1">
  <div class="view-content">
    <div class="item-list">    
      <ul>          
        <li class="views-row views-row-1 views-row-odd views-row-first">  
          <div class="views-field views-field-nothing">        
            <span class="field-content">
              <div class="author_image">
                <a target="_blank" href="<?php print "/users/" . $user->uid?>" >
                  <?php print $avatar?>
                </a>
              </div>
              <div class="comment_detail">
                <a href="<?php print "/users/" . $user->uid?>" ><?php print $profile->title?></a>
                <div class="comment_body"><?php print $comment->comment ?></div>
              </div>
              <div class="comment_info">
                <ul>
                  <li class="date"><?php print date('M d, Y \a\t g:i A', $comment->timestamp)?></li>
                  <li><a href="<?php print "/comment/reply/" . $comment->nid . '/' . $comment->cid ?>">Reply</a></li>
                  <li class="fb_like">
                    <iframe 
                      src="//www.facebook.com/plugins/like.php?href=<?php echo $url; ?>&amp;send=false&amp;layout=button_count&amp;width=75&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=20&amp;appId=748525238568379"
                      scrolling="no" 
                      frameborder="0" 
                      style="border:none; overflow:hidden; width: 75px; height: 20px;" 
                      allowTransparency="true">
                    </iframe>
                  </li>
                  <li><a>Report</a></li>
                </ul>
              </div>
            </span>  
          </div>
        </li>
      </ul>
    </div>    
  </div>
</div>
<!--<li class="views-row">
  <div class="views-field views-field-nothing">        
    <span class="field-content">
      <div class="author_image">
        <a target="_blank" href="<?php print "/users/" . $user->uid?>" ><?php print $avatar?></a>
      </div>
      <div class="comment_detail">
        <a href="<?php print "/users/" . $user->uid?>" ><?php print $profile->title?></a>
        <div class="comment_body"><?php print $comment->comment ?></div>
      </div>
      <div class="comment_info">
        <ul>
          <li class="date"><?php print date("M d, Y \a\t g:i A", $profile->timestamp)?></li>
          <li><a href="<?php print "/comment/reply/" . $comment->nid . '/' . $comment->cid ?>">Reply</a></li>
          <li class="fb_like">
            <iframe 
              src="//www.facebook.com/plugins/like.php?href=<?php echo $url; ?>&amp;send=false&amp;layout=button_count&amp;width=75&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=20&amp;appId=748525238568379"
              scrolling="no" 
              frameborder="0" 
              style="border:none; overflow:hidden; width: 75px; height: 20px;" 
              allowTransparency="true">
            </iframe>
          </li>
          <li><a>Report</a></li>
        </ul>
      </div>
    </span>  
  </div>
</li>-->
