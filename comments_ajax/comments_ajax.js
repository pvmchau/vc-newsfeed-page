(function ($) {
  Drupal.CTools = Drupal.CTools || {};
  Drupal.CTools.AJAX = Drupal.CTools.AJAX || {};
  Drupal.CTools.AJAX.commands = Drupal.CTools.AJAX.commands || {};
  Drupal.CTools.AJAX.commandCache = Drupal.CTools.AJAX.comandCache || {} ;
  Drupal.CTools.AJAX.scripts = {};
  Drupal.CTools.AJAX.css = {};
  
  Drupal.CTools.AJAX.commands.comment_append = function(data) {
    $(data.selector).append(data.data);
    Drupal.attachBehaviors($(data.selector));
    $("#" + data.form_id +  ' :input[name="comment"]').val('');
  };
})(jQuery);